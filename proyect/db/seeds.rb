# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Store.create(name: 'Hack Library', category: 'Librería y Papelería')
Admin.create(user_name: 'hackerman', password: '123321', store_id: 1)
Product.create(name: "Lapiz mongol 4k", description: "Lapiz mongol original mina ultra resistente para hacer dibujos HD y 4K.", price: 250, quantity: 50, store_id: 1)
Product.create(name: "Borrador nata", description: "Un borrador  corriente marca nata original. borra todos tus errores, pero no los corrige.", price: 1, quantity: 50, store_id: 1)
Product.create(name: "Pincel profesional", description: "Pincel profesional plano con bordes de oro e incrustaciones de diamantes.", price: 600.50, quantity: 50, store_id: 1)
Product.create(name: "Cuaderno de papel moneda", description: "Un cuaderno valioso para anotar informacion común", price: 245.99, quantity: 50, store_id: 1)
Product.create(name: "Sacapuntas de titanio", description: "el compañero perfecto para el Lapiz 4k", price: 95.00, quantity: 50, store_id: 1)