class ProductSerializer < ActiveModel::Serializer
  attributes :name, :image, :description, :price, :quantity

  belongs_to :store
end
