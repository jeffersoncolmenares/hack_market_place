class StoreSerializer < ActiveModel::Serializer
  attribute :name, key: :store
  attributes :category
  has_many :products
end
