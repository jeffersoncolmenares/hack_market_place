Rails.application.routes.draw do

  resources :stores
  resources :products

  namespace :api do
    namespace :v1 do
      resources :stores, shallow:true do   #el shallow aplica a lo que esta debajo.
        resources :products
        resources :admins
      end
    end
  end
  
end
